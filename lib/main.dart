import 'package:flutter/material.dart';
import 'views/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Weather App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.blue[800],
        hintColor: Colors.amber[600],
        scaffoldBackgroundColor: Colors.blueGrey[50],

        appBarTheme: AppBarTheme(
          color: Colors.blue[900],
          foregroundColor: Colors.white,
        ),

        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: Colors.blueGrey[900],
          selectedItemColor: Colors.amber[800],
          unselectedItemColor: Colors.grey[900],
        ),

        textTheme: TextTheme(
          headlineMedium: TextStyle(color: Colors.blueGrey[900], fontWeight: FontWeight.bold),
          titleMedium: TextStyle(color: Colors.blueGrey[600]),
          bodyMedium: TextStyle(color: Colors.blueGrey[800]),
        ),
      ),
      home: const HomePage(title: 'Flutter Weather Demo'),
    );
  }
}
