// weather daily model
class WeatherDaily {
  final DateTime date;
  final double temperature;
  final String description;

  WeatherDaily({
    required this.date,
    required this.temperature,
    required this.description,
  });

  factory WeatherDaily.fromJson(Map<String, dynamic> json) {
    return WeatherDaily(
      date: DateTime.fromMillisecondsSinceEpoch(json['dt'] * 1000, isUtc: true),
      temperature: json['temp']['day'].toDouble(),
      description: json['weather'][0]['description'],
    );
  }
}
