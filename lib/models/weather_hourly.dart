// weather hourly model
class WeatherHourly {
  final DateTime time;
  final double temperature;
  final String description;

  WeatherHourly({
    required this.time,
    required this.temperature,
    required this.description,
  });

  factory WeatherHourly.fromJson(Map<String, dynamic> json) {
    return WeatherHourly(
      time: DateTime.fromMillisecondsSinceEpoch(json['dt'] * 1000, isUtc: true),
      temperature: json['temp'].toDouble(),
      description: json['weather'][0]['description'],
    );
  }
}
