// favorites service
import 'package:shared_preferences/shared_preferences.dart';

class FavoritesService {
  Future<List<String>> getFavorites() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getStringList('favorites')?.toSet().toList() ?? [];
  }

  Future<void> addFavorite(String city) async {
    final prefs = await SharedPreferences.getInstance();
    final favorites = await getFavorites();
    if (!favorites.contains(city)) {
      favorites.add(city);
      await prefs.setStringList('favorites', favorites);
    }
  }

  Future<void> removeFavorite(String city) async {
    final prefs = await SharedPreferences.getInstance();
    final favorites = await getFavorites();
    favorites.remove(city);
    await prefs.setStringList('favorites', favorites);
  }

  Future<bool> isFavorite(String city) async {
    final favorites = await getFavorites();
    return favorites.contains(city);
  }
}
