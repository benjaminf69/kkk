// weather_service.dart is a service class that fetches weather data from the OpenWeatherMap API.
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/weather.dart';
import '../models/weather_hourly.dart';
import '../models/weather_daily.dart';

class WeatherService {
  final String apiKey = '204c5f2db69353082336ba8b943adaa3';
  final String baseUrl = 'https://api.openweathermap.org/data/2.5';
  final String newUrl = 'https://api.openweathermap.org/data/3.0';

  Future<Weather> fetchWeatherByLocation(double latitude, double longitude) async {
    final url = Uri.parse('$baseUrl/weather?lat=$latitude&lon=$longitude&appid=$apiKey&units=metric');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      return Weather.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load weather data');
    }
  }

  Future<Weather> fetchWeather(String city) async {
    final url = Uri.parse('$baseUrl/weather?q=$city&appid=$apiKey&units=metric');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      return Weather.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load weather data');
    }
  }

  Future<List<WeatherHourly>> fetchHourlyWeather(double latitude, double longitude) async {
    final url = Uri.parse('$newUrl/onecall?lat=$latitude&lon=$longitude&exclude=current,minutely,daily,alerts&appid=$apiKey&units=metric');
    final response = await http.get(url);
    print(response.body);
    if (response.statusCode == 200) {
      final List<dynamic> hourlyWeatherData = json.decode(response.body)['hourly'];
      return hourlyWeatherData.map((data) => WeatherHourly.fromJson(data)).toList();
    } else {
      throw Exception('Failed to load hourly weather data');
    }
  }

  Future<List<WeatherDaily>> fetchWeeklyWeather(double latitude, double longitude) async {
    final url = Uri.parse('$newUrl/onecall?lat=$latitude&lon=$longitude&exclude=current,minutely,hourly,alerts&appid=$apiKey&units=metric');
    final response = await http.get(url);
    print(response.body);
    print(url);
    if (response.statusCode == 200) {
      final List<dynamic> dailyWeatherData = json.decode(response.body)['daily'];
      return dailyWeatherData.map((data) => WeatherDaily.fromJson(data)).toList();
    } else {
      throw Exception('Failed to load daily weather data');
    }
  }
}
