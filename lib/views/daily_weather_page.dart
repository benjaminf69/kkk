import 'package:flutter/material.dart';
import '../models/weather_hourly.dart';
import '../services/weather_service.dart';

class DailyWeatherPage extends StatelessWidget {
  final double latitude;
  final double longitude;

  const DailyWeatherPage({super.key, required this.latitude, required this.longitude});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Météo Quotidienne'),
      ),
      body: FutureBuilder<List<WeatherHourly>>(
        future: WeatherService().fetchHourlyWeather(latitude, longitude),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          } else if (snapshot.hasError) {
            return const Text("Erreur de chargement des données météo");
          } else {
            return ListView.builder(
              itemCount: snapshot.data?.length ?? 0,
              itemBuilder: (context, index) {
                final hourlyWeather = snapshot.data![index];
                return ListTile(
                  title: Text("${hourlyWeather.time}: ${hourlyWeather.temperature}°C"),
                  subtitle: Text(hourlyWeather.description),
                );
              },
            );
          }
        },
      ),
    );
  }
}
