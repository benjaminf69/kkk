import 'package:flutter/material.dart';
import '../models/weather.dart';
import '../services/weather_service.dart';
import '../services/location_service.dart';
import '../services/favorites_service.dart';
import 'search_delegate.dart';
import 'random_weather_page.dart';
import 'daily_weather_page.dart';
import 'weekly_weather_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  late Future<Weather> _weatherData;
  double? _latitude;
  double? _longitude;
  late List<String> _favoriteCities = []; // Déclaration de _favoriteCities

  final FavoritesService _favoritesService = FavoritesService();

  @override
  void initState() {
    super.initState();
    _fetchCurrentLocationWeather();
    _loadFavoriteCities(); // Appel à la méthode pour charger les villes favorites
  }

  void _fetchCurrentLocationWeather() async {
    LocationService().getCurrentLocation().then((position) {
      setState(() {
        _latitude = position.latitude;
        _longitude = position.longitude;
        _weatherData = WeatherService().fetchWeatherByLocation(position.latitude, position.longitude);
      });
    }).catchError((err) {
      // Gestion des erreurs ou de l'impossibilité de récupérer la localisation
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("Erreur lors de la récupération de la localisation : $err")),
      );
    });
  }

  void _loadFavoriteCities() async {
    final favorites = await _favoritesService.getFavorites();
    setState(() {
      _favoriteCities = favorites;
    });
  }

  void _onItemTapped(int index) {
    if (_latitude != null && _longitude != null) { // Vérifiez d'abord la disponibilité de la localisation
      setState(() {
        _selectedIndex = index;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text("Localisation non disponible. Impossible d'afficher les prévisions météo.")),
      );
    }
  }


  List<Widget> _buildPageList() {
    return [
      FutureBuilder<Weather>(
        future: _weatherData,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          } else if (snapshot.hasError || !snapshot.hasData) {
            return const Text("Erreur de chargement des données météo");
          } else {
            // Display current weather details
            final weather = snapshot.data!;
            return Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('${weather.city}: ${weather.temperature.toStringAsFixed(1)}°C', style: Theme.of(context).textTheme.headline4),
                  Text(weather.description, style: Theme.of(context).textTheme.subtitle1),
                ],
              ),
            );
          }
        },
      ),
      const RandomWeatherPage(),
      _latitude != null && _longitude != null ? DailyWeatherPage(latitude: _latitude!, longitude: _longitude!) : const Text("Chargement de la localisation..."),
      _latitude != null && _longitude != null ? WeeklyWeatherPage(latitude: _latitude!, longitude: _longitude!) : const Text("Chargement de la localisation..."),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () async {
              final city = await showSearch(context: context, delegate: CitySearch());
              if (city != null) {
                setState(() {
                  _weatherData = WeatherService().fetchWeather(city);
                });
              }
            },
          ),
          IconButton( // Nouveau IconButton pour ajouter une ville en favori
            icon: Icon(Icons.favorite),
            onPressed: () async {
              final city = await showSearch(context: context, delegate: CitySearch());
              if (city != null) {
                setState(() {
                  _favoritesService.addFavorite(city); // Ajouter la ville en favori
                });
              }
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildFavoriteCitiesPanel(),
            Expanded(
              child: _buildPageList().elementAt(_selectedIndex),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Accueil',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shuffle),
            label: 'Aléatoire',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.today),
            label: 'Quotidien',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            label: 'Hebdomadaire',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        backgroundColor: Colors.blueGrey[900],
        onTap: _onItemTapped,
      ),
    );
  }

  Widget _buildFavoriteCitiesPanel() {
    return SizedBox(
      height: 50,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _favoriteCities.length,
        itemBuilder: (context, index) {
          final city = _favoriteCities[index];
          return GestureDetector(
            onTap: () {
              setState(() {
                _selectedIndex = 0;
                _weatherData = WeatherService().fetchWeather(city);
              });
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Chip(
                label: Text(city),
                backgroundColor: Colors.blue,
              ),
            ),
          );
        },
      ),
    );
  }
}
