import 'package:flutter/material.dart';
import '../models/weather.dart';
import '../services/weather_service.dart';
import '../widgets/weather_card.dart';

class RandomWeatherPage extends StatefulWidget {
  const RandomWeatherPage({super.key});

  @override
  _RandomWeatherPageState createState() => _RandomWeatherPageState();
}

class _RandomWeatherPageState extends State<RandomWeatherPage> {
  final List<String> _cities = ['Paris', 'Tokyo', 'New York', 'Sydney', 'Moscow', 'Cairo', 'Cape Town', 'Rio de Janeiro'];
  List<Future<Weather>> _weatherFutures = [];

  @override
  void initState() {
    super.initState();
    _weatherFutures = _cities.map((city) => WeatherService().fetchWeather(city)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 2 / 1,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
      ),
      padding: const EdgeInsets.all(10),
      itemCount: _weatherFutures.length,
      itemBuilder: (context, index) {
        return FutureBuilder<Weather>(
          future: _weatherFutures[index],
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return const Text("Erreur lors du chargement des données");
            } else {
              return WeatherCard(
                weather: snapshot.data!,
                onTap: () {},
              );
            }
          },
        );
      },
    );
  }
}
