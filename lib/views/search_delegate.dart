import 'package:flutter/material.dart';
import '../models/weather.dart';
import '../services/weather_service.dart';
import '../services/favorites_service.dart';

class CitySearch extends SearchDelegate<String> {
  final FavoritesService _favoritesService = FavoritesService();

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, '');
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.isEmpty) {
      return const Center(child: Text("Veuillez entrer le nom d'une ville."));
    }
    return FutureBuilder<Weather>(
      future: WeatherService().fetchWeather(query),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError || !snapshot.hasData) {
          return Center(child: Text("Aucun résultat trouvé pour '$query'."));
        }
        final weather = snapshot.data!;
        return ListTile(
          title: Text(weather.city),
          subtitle: Text("${weather.temperature.toStringAsFixed(1)}°C, ${weather.description}"),
          onTap: () {
            _handleCitySelection(weather.city);
            close(context, weather.city);
          },
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestions = ["Paris", "Lyon", "Marseille", "Toulouse"];

    return ListView.builder(
      itemCount: suggestions.length,
      itemBuilder: (context, index) => ListTile(
        title: Text(suggestions[index]),
        onTap: () {
          query = suggestions[index];
          _handleCitySelection(query);
          showResults(context);
        },
      ),
    );
  }

  void _handleCitySelection(String city) async {
    final isFavorite = await _favoritesService.isFavorite(city);
    if (!isFavorite) {
      await _favoritesService.addFavorite(city);
    }
  }
}
