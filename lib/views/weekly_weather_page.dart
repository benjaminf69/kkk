import 'package:flutter/material.dart';
import '../models/weather_daily.dart';
import '../services/weather_service.dart';

class WeeklyWeatherPage extends StatelessWidget {
  final double latitude;
  final double longitude;

  const WeeklyWeatherPage({super.key, required this.latitude, required this.longitude});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Météo Hebdomadaire'),
      ),
      body: FutureBuilder<List<WeatherDaily>>(
        future: WeatherService().fetchWeeklyWeather(latitude, longitude),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          } else if (snapshot.hasError) {
            return const Text("Erreur de chargement des données météo");
          } else {
            return ListView.builder(
              itemCount: snapshot.data?.length ?? 0,
              itemBuilder: (context, index) {
                final dailyWeather = snapshot.data![index];
                return ListTile(
                  title: Text("${dailyWeather.date}: ${dailyWeather.temperature}°C"),
                  subtitle: Text(dailyWeather.description),
                );
              },
            );
          }
        },
      ),
    );
  }
}
