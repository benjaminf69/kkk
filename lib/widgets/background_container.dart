import 'package:flutter/material.dart';
import '../models/weather.dart';

class BackgroundContainer extends StatelessWidget {
  final Weather? weather;
  final Widget? child;

  const BackgroundContainer({Key? key, this.weather, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String backgroundImage = 'sunny.jpg';
    if (weather != null) {
      if (weather!.description.contains('rain')) {
        backgroundImage = 'rainy.jpg';
      } else if (weather!.description.contains('cloud')) {
        backgroundImage = 'cloudy.jpg';
      }
    }

    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/$backgroundImage"),
          fit: BoxFit.cover,
        ),
      ),
      child: child != null ? Align(alignment: Alignment.center, child: child) : Container(),
    );
  }
}
