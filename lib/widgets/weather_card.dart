import 'package:flutter/material.dart';
import '../models/weather.dart';
import '../services/favorites_service.dart';

class WeatherCard extends StatefulWidget {
  final Weather weather;
  final VoidCallback onTap;

  const WeatherCard({Key? key, required this.weather, required this.onTap}) : super(key: key);

  @override
  _WeatherCardState createState() => _WeatherCardState();
}

class _WeatherCardState extends State<WeatherCard> {
  final FavoritesService _favoritesService = FavoritesService();
  bool _isFavorite = false;

  @override
  void initState() {
    super.initState();
    _checkIfFavorite();
  }

  void _checkIfFavorite() async {
    final result = await _favoritesService.isFavorite(widget.weather.city);
    setState(() {
      _isFavorite = result;
    });
  }

  void _toggleFavorite() async {
    if (_isFavorite) {
      await _favoritesService.removeFavorite(widget.weather.city);
    } else {
      await _favoritesService.addFavorite(widget.weather.city);
    }
    setState(() {
      _isFavorite = !_isFavorite;
    });
  }

  @override
  Widget build(BuildContext context) {
    String backgroundImage = getBackgroundImage(widget.weather.description);

    return GestureDetector(
      onTap: widget.onTap,
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/$backgroundImage"),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.darken),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                // Utilisez Expanded pour que le texte utilise l'espace disponible
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        widget.weather.city,
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(color: Colors.white),
                      ),
                      Text(
                        '${widget.weather.temperature.toStringAsFixed(1)}°C',
                        style: Theme.of(context).textTheme.headlineSmall?.copyWith(color: Colors.white),
                      ),
                      Text(
                        widget.weather.description,
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Colors.white),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    icon: Icon(_isFavorite ? Icons.favorite : Icons.favorite_border),
                    color: Colors.red,
                    onPressed: _toggleFavorite,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

    String getBackgroundImage(String description) {
    if (description.contains('rain')) {
      return 'rainy.jpg';
    } else if (description.contains('cloud')) {
      return 'cloudy.jpg';
    } else if (description.contains('clear')) {
      return 'sunny.png';
    } else if (description.contains('sun')) {
      return 'sunny.png';
    } else {
      return 'default.png';
    }
  }
}
